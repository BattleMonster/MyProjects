import java.io.*;
import java.util.Calendar;
import java.util.Properties;

class PropertiesFunc {
    private Properties properties = new Properties();
    private File file = new File("E:\\Mel\\Test\\properties\\properties.txt");

    public void loadProp() throws IOException {
        properties.load(new FileInputStream(file));
    }

    public void propDefault() {
        Calendar calendar = Calendar.getInstance();
        properties.put("count", "1");
        properties.put("day", calendar.get(Calendar.DATE));
        properties.put("path", "null");
    }

    public void setCount(String num) throws IOException {
        properties.setProperty("count", num);
        properties.store(new FileOutputStream(file), null);
    }

    public void setDay(String day) throws IOException {
        properties.setProperty("day", day);
        properties.store(new FileOutputStream(file), null);
    }

    public void setPath(String path) throws IOException {
        properties.setProperty("path", path);
        properties.store(new FileOutputStream(file), null);
    }

    public String getCount() throws IOException {
        loadProp();
        String count = properties.getProperty("count");
        return count;
    }

    public String getDay() throws IOException {
        loadProp();
        String day = properties.getProperty("day");
        return day;
    }

    public String getPath() throws IOException {
        loadProp();
        String path = properties.getProperty("path");
        return path;
    }
}
