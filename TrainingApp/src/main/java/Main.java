import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        DateController dc = new DateController();
        dc.dayChecker();
        FileManager fm = new FileManager();
        PropertiesFunc pf = new PropertiesFunc();
        pf.loadProp();
        System.out.println("Count is: " + pf.getCount());
        System.out.println("Day is: " + pf.getDay());
        System.out.println("Path is: " + pf.getPath());
        fm.isFolderExist();
        fm.getFile();
    }
}
