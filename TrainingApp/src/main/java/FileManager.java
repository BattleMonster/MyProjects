import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class FileManager {
    private PropertiesFunc pf = new PropertiesFunc();
    private String path = pf.getPath();
    private Path path1 = Paths.get(path);

    FileManager() throws IOException {
    }

    public void isFolderExist() throws IOException {
        if (Files.isDirectory(path1)) {
            path = pf.getPath();
        } else {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Введите путь к папке с тренировками");
            path = scanner.nextLine();
            pf.setPath(path);
        }
        File file = new File(path);
        if (file.isDirectory()) {
            System.out.println("Путь принят");
        } else {
            System.out.println("Пути не существует");
        }
    }

    public void fileListGetter() {
        final File file = new File(path);
        final File[] files = file.listFiles();
        if (files != null) {
            for (File file1 : files) {
                System.out.println(file1);
            }
        }
    }

    public void getFile() throws IOException {
        java.awt.Desktop.getDesktop().open(new File(path + "\\" + Integer.parseInt(pf.getCount()) + ".docx"));
    }
}

