import java.io.IOException;
import java.util.Calendar;

public class DateController {
    public void dayChecker() throws IOException {
        PropertiesFunc pf = new PropertiesFunc();
        int count = Integer.parseInt(pf.getCount());
        if (Integer.parseInt(pf.getDay()) != currentDate()) {
            pf.setDay(Integer.toString(currentDate()));
            if (count < 30) {
                pf.setCount(Integer.toString(count + 1));
            } else {
                pf.setCount("1");
            }
        }
    }

    private int currentDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.DATE);
    }
}
