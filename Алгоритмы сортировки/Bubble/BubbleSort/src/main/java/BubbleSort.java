public class BubbleSort {
    public void startBubbleSort(int a[]) {
        for (int anA1 : a) {
            System.out.print(anA1 + " ");
        }
        for (int i = a.length - 1; i >= 1; i--) {
            for (int j = 0; j <= i - 1; j++) {
                if (a[j] > a[j + 1]) {
                    int tmp = 0;
                    tmp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = tmp;
                }
            }
        }
        System.out.println();
        for (int anA : a) {
            System.out.print(anA + " ");
        }
        System.out.println();
    }
    public void startBubbleSort(double a[]) {
        for (double anA1 : a) {
            System.out.print(anA1 + " ");
        }
        for (int i = a.length - 1; i >= 1; i--) {
            for (int j = 0; j <= i - 1; j++) {
                if (a[j] > a[j + 1]) {
                    double tmp = 0;
                    tmp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = tmp;
                }
            }
        }
        System.out.println();
        for (double anA : a) {
            System.out.print(anA + " ");
        }
    }
}
