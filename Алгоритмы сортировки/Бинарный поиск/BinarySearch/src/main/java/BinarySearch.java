public class BinarySearch {
    public int startBinarySearch(int a[], int search) {
        int low=0;
        int high = a.length-1;

        while (low<=high){
            int mid =(low+high)/2;
            int guess = a[mid];
            if (guess == search){
                return mid;
            } else if (guess>search){
                high=mid-1;
            }else {
                low=mid+1;
            }
        }
        return 0;
    }
}
