package Dogs;

public class Home extends Dogs {
    private boolean Beagle=false;
    private String HomeType;

    public boolean isBeagle() {
        return Beagle;
    }

    public void setBeagle(boolean isbeagle) {
        Beagle = isbeagle;
    }

    public String getHomeType() {
        return HomeType;
    }

    public void setHomeType(String homeType) {
        HomeType = homeType;
    }
}
