package Dogs;

public class Hunting extends Home {
    private boolean isTeachedForHunting;

    public boolean isTeachedForHunting() {
        return isTeachedForHunting;
    }

    public void setTeachedForHunting(boolean isteachedForHunting) {
        isTeachedForHunting = isteachedForHunting;
    }
}
