package Dogs;

import java.util.ArrayList;

public class Dogs extends Mammals implements SenceOfSmellAction {
    private ArrayList<String> HuntingSkills = new ArrayList();
    private int lengtOfTail;
    private String SenseOfSmell = "";

    public void setSenseOfSmell(String senseOfSmell) {
        SenseOfSmell = senseOfSmell;
    }

    public String getHuntingSkils() {
        String result = "";
        for (int i = 0; i < HuntingSkills.size(); i++) {
            result = HuntingSkills.get(i);
        }
        return result;
    }

    public void setHuntingSkils(String huntingSkils) {
        HuntingSkills.add(huntingSkils);
    }

    public int getLengtOfTail() {
        return lengtOfTail;
    }

    public void setLengtOfTail(int lengtOfTail) {
        this.lengtOfTail = lengtOfTail;
    }


    @Override
    public String SenceOfSmellActin() {
        String result = "";
        if (SenseOfSmell.equals("Cat")) {
            result = "Chase";
        } else if (SenseOfSmell.equals("Food")) {
            result = "Go to food";
        }
        return result;
    }
}
