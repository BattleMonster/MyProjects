package MyDogs;

import Dogs.Labrador;

public class Kelly {
    Labrador labrador = new Labrador();

    public void setParams() {
        labrador.setAge(5);
        labrador.setSex("female");
        labrador.setWeight(10);
        labrador.setNumbofkids(0);
        labrador.setPregnansyPeriod(8);
        labrador.setHuntingSkils("Search for prey");
        labrador.setHuntingSkils("Dragging the booty");
        labrador.setLengtOfTail(12);
        labrador.setHomeType("Home");
        labrador.setBeagle(true);
        labrador.setTeachedForHunting(true);
        labrador.setPresenceOfPedigree(false);
        labrador.setSenseOfSmell("Food");
    }

    public void describe() {
        System.out.printf("Age: %s %n Sex: %s %n Weight: %s %n Numer of kids: %s %n Pregnansy period: %s %n Hunting skills: %s %n Length of tail: %s %n Home type: %s %n Beagle: %s %n " +
                        "Theached for hunting: %s %n Presence of pedigree: %s %n Odor reaction: %s",
                labrador.getAge(),
                labrador.getSex(),
                labrador.getWeight(),
                labrador.getNumbofkids(),
                labrador.getPregnansyPeriod(),
                labrador.getHuntingSkils(),
                labrador.getLengtOfTail(),
                labrador.getHomeType(),
                labrador.isBeagle(),
                labrador.isTeachedForHunting(),
                labrador.isPresenceOfPedigree(),
                labrador.SenceOfSmellActin()
        );
    }
}
